#include <stdio.h>
#include <stdbool.h>

// Determines frequency of first scanned integer in a list of integer along with the position of its appearances.
int main()
{

  int initial, number;
  int n = 0;
  int frequency = 0;
  int count = 0;
  bool numbersFound = false;
  bool initialRun = true;

  printf("Running program to count frequency of first scanned integer.. \n");

  // Goes into our loop
  while (true) {

    // Scan next line
    n = scanf("%i", &number);

    // Check if the program has successfully scanned for a number. If it has, then assign the scanned number to the initial number.
    if (n > 0)
    {
      if (!numbersFound)
      {
        numbersFound = true;
        initial = number;
      }

      // Increments frequency and gives output if number found is equal to the initial number scanned
      if (number == initial && !initialRun)
      {
        printf("Number %i was found at position: %i\n", initial, count);
        frequency++;
      };

      if (initialRun)
      {
        initialRun = false;
      }

      // Only increment count (position) if we encounter a number
      count++;
    }

    // Flush stdin to remove garbage input
    else
    {
      n = getc(stdin);

      // End loop if at end of file
      if (n == EOF)
      {
        if (!numbersFound)
        {
          printf("There are no numbers in this file. \n");
          return 0;
        }

        break;
      }

      while (n != '\n' && n != EOF)
      {
          n = getc(stdin);
      }
    }
  }

  // Prints the frequency of the number found.
  printf("Number %i was found %i times.\n", initial, frequency);

  return 0;

}
