#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "pt_utils.h"

int pt_rand(int nbits) {
  int mask;
  if (0 < nbits && nbits < sizeof(int)*8) {
    // the least significant nbits bits will be 1, others will be 0
    mask = ~( ~( (unsigned int) 0) << nbits );
  }
  else {
    // the mask will be all ones
    mask = ~((unsigned int) 0);
  }
  // get the next random number and keep only nbits bits
  return rand() & mask;
}

int * gen_rand_int_array(int length, int nbits) {

  int * a = malloc(sizeof(int)*length);
  for (int i = 0; i < length; i++) {
    a[i] = pt_rand(nbits);
  }
  return a;
}

void copy_int_array(int src[], int dst[], int length) {

  for (int i = 0; i < length; i++) {
    dst[i] = src[i];
  }
}

int * clone_int_array(int a[], int length) {

  int * clone = malloc(sizeof(int)*length);
  copy_int_array(a, clone, length);

  return clone;
}

void print_int_array(int a[], int length) {

  for (int i = 0; i < length; i++) {
    printf("%d ", a[i]);
  }
  printf("\n");
}

void swap_int(int* int1, int* int2)
{
  int temp = *int1;
  *int1 = *int2;
  *int2 = temp;
}

void shuffle_int_array(int a[], int length)
{
  for (int i = length - 1; i >= 1; i--)
  {
    int j = pt_rand(log2(length));
    swap_int(&a[i], &a[j]);
  }
}

int linear_search(int a[], int length, int target)
{
  int i = 0;

  while (a[i] != target)
  {
    i++;
    if (i > length) return -1;
  }

  return i;
}

int binary_search(int a[], int length, int target)
{
  int left = 0;
  int right = length - 1;
  int middle = floor((left + right) / 2) ;

  while (left <= right)
  {
    middle = floor((left + right) / 2);

    if (a[middle] == target && a[middle-1] != target) return middle;

    else if (a[middle] < target)
    {
      left = middle + 1;
    }
    else
    {
      right = middle - 1;
    }
  }

  return -1;
}
