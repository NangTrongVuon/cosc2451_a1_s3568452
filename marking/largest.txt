#### score: 9/10


Testing largest
****************************************

test: 1a.txt
****************************************
Running program to determine largest number..
Largest: 64
****************************************
diff with good:
1,2c1
< Running program to determine largest number..
< Largest: 64
---
> The largest number is: 64
****************************************
#### output very far from sample output
#### -1

test: 1b.txt
****************************************
Running program to determine largest number..
Largest: 64
****************************************
diff with good:
1,2c1
< Running program to determine largest number..
< Largest: 64
---
> The largest number is: 64
****************************************


test: 1c.txt
****************************************
Running program to determine largest number..
No numbers found in this file.
****************************************
diff with good:
1,2c1
< Running program to determine largest number..
< No numbers found in this file.
---
> No numbers read, cannot find the largest.
****************************************


test: 1d.txt
****************************************
Running program to determine largest number..
No numbers found in this file.
****************************************
diff with good:
1,2c1
< Running program to determine largest number..
< No numbers found in this file.
---
> No numbers read, cannot find the largest.
****************************************


test: 1e.txt
****************************************
Running program to determine largest number..
Largest: 54
****************************************
diff with good:
1,2c1
< Running program to determine largest number..
< Largest: 54
---
> The largest number is: 54
****************************************


test: 1f.txt
****************************************
Running program to determine largest number..
Largest: 54
****************************************
diff with good:
1,2c1
< Running program to determine largest number..
< Largest: 54
---
> The largest number is: 54
****************************************


test: 1g.txt
****************************************
Running program to determine largest number..
Largest: 54
****************************************
diff with good:
1,2c1
< Running program to determine largest number..
< Largest: 54
---
> The largest number is: 54
****************************************


test: 1h.txt
****************************************
Running program to determine largest number..
Largest: 222
****************************************
diff with good:
1,2c1
< Running program to determine largest number..
< Largest: 222
---
> The largest number is: 222
****************************************


test: 1i.txt
****************************************
Running program to determine largest number..
Largest: 222
****************************************
diff with good:
1,2c1
< Running program to determine largest number..
< Largest: 222
---
> The largest number is: 222
****************************************


test: 1j.txt
****************************************
Running program to determine largest number..
Largest: 54
****************************************
diff with good:
1,2c1
< Running program to determine largest number..
< Largest: 54
---
> The largest number is: 54
****************************************


test: 1.txt
****************************************
Running program to determine largest number..
Largest: 64
****************************************
diff with good:
1,2c1
< Running program to determine largest number..
< Largest: 64
---
> The largest number is: 64
****************************************


test: 2.txt
****************************************
Running program to determine largest number..
No numbers found in this file.
****************************************
diff with good:
1,2c1
< Running program to determine largest number..
< No numbers found in this file.
---
> No numbers read, cannot find the largest.
****************************************


test: 3.txt
****************************************
Running program to determine largest number..
Largest: -21
****************************************
diff with good:
1,2c1
< Running program to determine largest number..
< Largest: -21
---
> The largest number is: -21
****************************************


test: 4.txt
****************************************
Running program to determine largest number..
Largest: 23
****************************************
diff with good:
1,2c1
< Running program to determine largest number..
< Largest: 23
---
> The largest number is: 23
****************************************


test: 5.txt
****************************************
Running program to determine largest number..
Largest: 10
****************************************
diff with good:
1,2c1
< Running program to determine largest number..
< Largest: 10
---
> The largest number is: 10
****************************************


