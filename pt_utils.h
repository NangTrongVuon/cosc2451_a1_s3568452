#ifndef PT_UTILS
#define PT_UTILS

/** Generate a random int between 0 and (2^nbits)-1
  * @param nbits the number of bits to use
  */
int pt_rand(int nbits);

/** Generate an array containing random int.
  * The array will be allocated on the heap.
  * @param length the length of the array
  * @param nbits the number of bits to pass to pt_rand
  */
int * gen_rand_int_array(int length, int nbits);

/** Copy the source array into the destination.
  * Assumes there is enough space in the destination to hold everything.
  * @param src the source array
  * @param dst the destination array
  * @param length the number of elements to copy
  */
void copy_int_array(int src[], int dst[], int length);

/** Clone an array.
  * The cloned array will be allocated on the heap.
  * @param a the array to clone
  * @param length the length of the array
  */
int * clone_int_array(int a[], int length);

/** Print an array of int.
  * @param a the array to print
  * @param length the length of the array
  */
void print_int_array(int a[], int length);

/**
 * Swaps two integers.
 * @param int1 Pointer to the first integer swapped.
 * @param int2 Pointer to the second integer swapped.
 */
void swap_int(int* int1, int* int2);

/**
 * Shuffles an array in place using the modern Fisher-Yates shuffle.
 * @param a the array to shuffle
 * @param length the length of the array
 */
void shuffle_int_array(int a[], int length);

/**
 * Performs a linear search on the array with a target specified.
 * @param a the array to perform search in
 * @param length the length of the array
 * @param target the target to search for
 * @return
 */
int linear_search(int a[], int length, int target);

/**
 * Performs a binary search on the array with a target specified.
 * @param a the array to perform search in
 * @param length the length of the array
 * @param target the target to search for
 * @return
 */
int binary_search(int a[], int length, int target);

#endif
