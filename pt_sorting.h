#ifndef PT_SORTING
#define PT_SORTING

#define N_SORT_FUNCS 5

/** Sort an int array using the selection sort algorithm.
  * Sort function number: 1
  * @param a the int array to be sorted in place
  * @param length the length of array a
  */
void selection_sort(int a[], int length);

/** Sort an int array using the insertion sort algorithm.
  * Sort function number: 2
  * @param a the int array to be sorted in place
  * @param length the length of array a
  */
void insertion_sort(int a[], int length);

/** Sort an int array using the merge sort algorithm.
  * Sort function number: 3
  * @param a the int array to be sorted in place
  * @param length the length of array a
  */
void merge_sort(int a[], int length);

/** Sort an int array using the heap sort algorithm.
  * Sort function number: 4
  * @param a the int array to be sorted in place
  * @param length the length of array a
  */
void heap_sort(int a[], int length);

/** Sort an int array using the quicksort algorithm.
  * Sort function number: 5
  * @param a the int array to be sorted in place
  * @param length the length of array a
  */
void quick_sort(int a[], int length);


#endif
