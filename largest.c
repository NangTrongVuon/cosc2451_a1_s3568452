#include <stdio.h>
#include <stdbool.h>

// Determines largest number in a sequence of integers
int main() {

  int largest, number, n;
  bool numbersFound = false;

  printf("Running program to determine largest number..\n");

  while (true)
  {
    n = scanf("%i", &number);

    // Check if number scanned is bigger than last number
    if (n > 0)
    {
      // Check if the program has successfully scanned for a number. If it has, then assign the initial largest number.
        if (!numbersFound)
        {
          numbersFound = true;
          largest = number;
        }

      if (number > largest)
      {
        largest = number;
      };
    }

    else
    {
      n = getc(stdin);

      // End loop if at end of file
      if (n == EOF)
      {
        if (!numbersFound)
        {
          printf("No numbers found in this file.\n");
          return 0;
        }

        break;
      }

      // Flush stdin if input is invalid
      while (n != '\n' && n != EOF)
      {
          n = getc(stdin);
      }
    }
  }

  printf("Largest: %i\n", largest);
  return 0;

}
