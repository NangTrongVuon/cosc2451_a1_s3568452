#include <stdio.h>
#include <time.h>
#include "pt_utils.h"

int array[] = {1, 2, 5, 7, 9, 20};

int main() {
    srand((unsigned int) time(NULL));
    // shuffle_int_array(array, 6);

    for (int i = 0; i < 6; i++)
    {
        printf("%i ", array[i]);
    }

    printf("\n");

    int result = linear_search(array, 6, 20);
    printf("linear search found target at index: %i\n \n", result);

    int binary_result = binary_search(array, 6, 20);
    printf("binary search found target at index: %i\n", binary_result);

    return 0;
}
