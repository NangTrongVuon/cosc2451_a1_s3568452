#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "pt_utils.h"
#include "pt_sorting.h"


int main(int argc, char * argv[]) {

  // Length: length of array
  int length;
  int nbits = 10;
  int iterations;

  if (argc < 3)
  {
    printf("Please enter at least 3 arguments.\n");
    return 0;
  }

  if (argc >= 1)
  {
    length = atoi(argv[1]);
  }

  // Uses 3rd argument as seed
  if (argc >= 2)
  {
    // length = atoi(argv[1]);
    srand(atoi(argv[2]));
  }

  // Uses 4th argument as iteration count
  if (argc >= 3)
  {
    iterations = atoi(argv[3]);
  }

  // if (argc >= 4)
  // {
  //     if (argv[3][0] == 't') {
  //         time_t current_time = time(NULL);
  //         srand(current_time);
  //         printf("time: %s\n", ctime(&current_time));
  //     }
  //     else {
  //         srand(atoi(argv[3]));
  //     }
  // }

  int * a = gen_rand_int_array(length, nbits);
  // int * clone = clone_int_array(a, length);

  // int target = pt_rand(10);

  int target = 505;

  selection_sort(a, length);

  print_int_array(a, length);

  /*    print_int_array(clone, length);*/

  time_t start_t;
  clock_t start_c;

  time_t end_t;
  clock_t end_c;

  float linear_total = 0;
  float binary_total = 0;

  for (int i = 0; i < iterations; i++)
  {

    target = pt_rand(10);

    printf("current target: %i\n", target);

    start_t = time(NULL);
    start_c = clock();

    // printf("Running linear search.. \n");

    printf("linear search returned %i\n",linear_search(a, length, target));

    end_t = time(NULL);
    end_c = clock();

    linear_total += end_c - start_c;

    start_t = time(NULL);
    start_c = clock();

    // printf("Running binary search.. \n");

    printf("binary search returned %i\n",binary_search(a, length, target));

    end_t = time(NULL);
    end_c = clock();

    binary_total += end_c - start_c;

  }

  double linear_average = linear_total / iterations;
  double binary_average = binary_total / iterations;

  printf("linear clock average: %f\n", linear_average);
  printf("binary clock average: %f\n", binary_average);

  // printf("Time: %f\n", difftime(end_t, start_t));
  // printf("Clock (micros): %ld\n", end_c-start_c);
  // printf("Clock (ms): %f\n", (end_c-start_c)/1000.0);
  // printf("Clock (s): %f\n", (end_c-start_c)/1000000.0);
  //
  // printf("\n");

  FILE* f = fopen("algorithmSpeed.csv", "a");
  fprintf(f, "%f %f\n", linear_average, binary_average);
  fclose(f);

  // free(clone);
  free(a);
}
